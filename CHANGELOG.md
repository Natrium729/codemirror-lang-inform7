# Changelog

## 0.2.0

- Update for CodeMirror 6 0.20.0.

## 0.1.0

- Initial release. Contains a basic but functional Inform 7 syntax highlighter for CodeMirror 6.
