import typescript from "rollup-plugin-ts"
import { nodeResolve } from "@rollup/plugin-node-resolve";

export default {
	input: "preview/preview.ts",
	output: {
		file: "preview/preview.bundle.js",
		format: "iife",
	},
	plugins: [typescript(), nodeResolve()]
}
